feat:
	PYTHONPATH=./src python -m detect_featuring.featuring_knn_model

reg-knn:
	PYTHONPATH=./src python -m detect_featuring.detect

evaluation:
	PYTHONPATH=./src python -m evaluation.evaluate

clear-pyc:
	find . | grep -E "(__pycache__|\.pyc|\.pyo$$)" | xargs rm -rf

setup:
	pip install -r requirements.txt
