## MM 20190716
- Reorganize directory, should be a noun
- Rename files, filename should be a noun, too
- Fix all the linting error
- Strengthen FaceRecognition class
- Code as python module
- Make script/Preprocessor class to preprocess/normalize the train datas
- Use different data for test and train
- Generalize the detection and recognition process
