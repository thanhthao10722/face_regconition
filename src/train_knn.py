import math
from sklearn import neighbors
import os
import os.path
import pickle
from PIL import Image, ImageDraw
import face_recognition
from face_recognition.face_recognition_cli import image_files_in_folder


class KnnTrain:
    train_dir=None
    model_path=None
    n_neighbors=None
    knn_algo='ball_tree'
    verbose=False
    distance_threshold = 0.3

    def __init__(self, train_dir, model_path, n_neighbors):
        self.train_dir = train_dir
        self.model_path = model_path
        self.n_neighbors = n_neighbors

    def train(self):
        X = []
        Y = []

        print("Detect and Encoding image....")
        for class_dir in os.listdir(self.train_dir):
            if not os.path.isdir(os.path.join(self.train_dir, class_dir)):
                continue

            for img_path in image_files_in_folder(
                os.path.join(self.train_dir, class_dir)
            ):
                image = face_recognition.load_image_file(img_path)

                face_encodings = face_recognition.face_encodings(
                    image, num_jitters=0)
                if len(face_encodings) < 1:
                    continue
                else:
                    X.append(face_encodings[0])
                    Y.append(class_dir)
        print("Finished encoding ...")

        if self.n_neighbors is None:
            self.n_neighbors = int(round(math.sqrt(len(X))))
            if self.verbose:
                print("Chose n_neighbors automatically:", self.n_neighbors)

        knn_clf = neighbors.KNeighborsClassifier(
            n_neighbors=self.n_neighbors, algorithm=self.knn_algo, weights='distance')
        knn_clf.fit(X, Y)
        if self.model_path is not None:
            with open(self.model_path, 'wb') as f:
                pickle.dump(knn_clf, f)

        return knn_clf



