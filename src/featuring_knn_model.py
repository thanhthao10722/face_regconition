from knn import KnnTrain
import os 

if __name__== "__main__":
    print("Starting....")
    neighbor = 8
    classifier = KnnTrain("data/train", "data/model/knn.clf", neighbor)
    classifier.train()
    print("Training Complete!")      