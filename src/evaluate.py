import os
import os.path
import pickle
from face_recognition.face_recognition_cli import image_files_in_folder
from PIL import Image, ImageDraw
import face_recognition

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

class Evaluation:
    model_path=None
    distance_threshold=0.3

    def __init__(self, model_path):
       self.model_path = model_path

    def predict(self, X_img_path, knn_clf=None, distance_threshold=0.3):
        # wrap this condition into a function
        if not os.path.isfile(X_img_path) or \
                os.path.splitext(X_img_path)[1][1:] not in ALLOWED_EXTENSIONS:
            raise Exception("Invalid image path: {}".format(X_img_path))

        if knn_clf is None and self.model_path is None:
            raise Exception(
                "Must supply knn classifier either thourgh knn_clf or model_path")
        if knn_clf is None:
            with open(self.model_path, 'rb') as f:
                knn_clf = pickle.load(f)

        X_img = face_recognition.load_image_file(X_img_path)
        X_face_locations = face_recognition.face_locations(X_img)

        if len(X_face_locations) == 0:
            return []

        faces_encodings = face_recognition.face_encodings(
            X_img, known_face_locations=X_face_locations)

        closest_distances = knn_clf.kneighbors(faces_encodings, n_neighbors=10)
        # verbose this
        are_matches = [closest_distances[0][i][0] <=
                       distance_threshold for i in range(len(X_face_locations))]
        # verbose this
        return [(pred, loc) if rec else ("unknown", loc) for pred, loc, rec in zip(
            knn_clf.predict(faces_encodings), X_face_locations, are_matches
        )]

    # should evaluate on one person only
    def evaluation(self, evaluate_dir):
        wrong = 0
        right = 0

        for class_dir in os.listdir(evaluate_dir):
            if not os.path.isdir(os.path.join(evaluate_dir, class_dir)):
                continue
            for img_path in image_files_in_folder(os.path.join(evaluate_dir, class_dir)):
                faces = self.predict(img_path)
                if faces:
                    face = faces[0]
                    face_name = face[0]
                    if face_name == class_dir:
                        right = right + 1
                    else:
                        wrong = wrong + 1
        return right / (wrong + right) * 100

if __name__ == "__main__":
    eval = Evaluation("data/model/knn.clf")
    print("Precision: %s" % eval.evaluation("data/test"))


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              