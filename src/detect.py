import cv2
import face_recognition
import time
import logging
import pickle
import json
from PIL import Image, ImageDraw


# rename to FaceRecognition
class Detection:
    # assign default value for some of the common arguments
    def __init__(self, model_path, video_path, thres_hold, num_frame, num_jitters, faceDetectionModel):
        self.model_path = model_path
        self.video_path = video_path
        self.thres_hold = thres_hold # check typo
        self.num_frame =  num_frame
        self.num_jitters = num_jitters
        self.faceDetectionModel = faceDetectionModel

    def detection(self):
        video_capture = cv2.VideoCapture(self.video_path)
        with open(self.model_path, "rb") as f:
            knn_clf = pickle.load(f)

        while True:
            ret, frame = video_capture.read()
            if not ret:
                break
            image = frame.copy()
            self.num_frame +=1
            if self.num_frame > 1:
                frame = frame[:, :, ::-1]
                face_locations = face_recognition.face_locations(frame, model=self.faceDetectionModel)
                # seperate into 3 steps:
                # 1. detection and tranform face images (eg:: convert to grayscale)
                # 2. recognition
                # 3. Output result and images to screen

                if(len(face_locations) == 0):
                    logging.info('No face is detected')
                else:
                    # represent face as 128...
                    face_encodings = face_recognition.face_encodings(frame, face_locations, num_jitters=self.num_jitters)
                    closest_distance = knn_clf.kneighbors(face_encodings, n_neighbors=1)
                    matches = [closest_distance[0][i][0] <= self.thres_hold for i in range(len(face_encodings))]
                    predictions = []

                    for pred, loc, rec in zip(knn_clf.predict(face_encodings), face_locations, matches):
                        item = (pred, loc) if rec else("Unknown", loc)
                        predictions.append(item)

                    for name, (top, right, bottom, left) in predictions:
                        cv2.rectangle(image, (left, top), (right, bottom), (0, 255, 0), 3)
                        cv2.rectangle(image, (left, bottom - 35), (right, bottom), (0, 255, 0), cv2.FILLED)
                        font = cv2.FONT_HERSHEY_DUPLEX
                        cv2.putText(image, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

                cv2.imshow('Face-Regconition', image)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        cv2.destroyAllWindows()
        time_now = time.strftime("%m/%d/%Y, %H:%M:%S", time.localtime())
        img_path = 'log/image-detect/' + str(time.time()) + '.png'
        cv2.imwrite(img_path, image)

        face  = predictions[0]
        face_name = face[0]
        
        f = open("log/result-detect/result.txt", "a")
        result = {
            "name":face_name,
            "time":time_now,
            "image_path": img_path
        }
        f.write(json.dumps(result)+"\n") 
        # remember to close the file

if __name__=="__main__":
    detect = Detection("data/model/knn.clf", 0, 0.3, 0, 0, "hog")
    detect.detection()
